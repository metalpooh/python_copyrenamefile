# -*- coding: utf-8 -*-
__author__ = 'metal'
import os
import shutil
import csv

shcode = []

def get_CSV():
    with open('StockMaster0821_code.csv','rb') as csvfile:
        csvReader = csv.reader(csvfile,delimiter = ',', quotechar = '|' )
        for row in csvReader:
            #name = row[0].decode('cp949')
            # 종목 Code 를 가지고 오는 Source
            code = row[0][1:]

            shcode.append(code)

    changeName()

def copy_rename(old_file_name, new_file_name):
        src_dir= os.curdir
        dst_dir= os.path.join(os.curdir , "newSave")
        src_file = os.path.join(src_dir, old_file_name)
        shutil.copy(src_file,dst_dir)

        dst_file = os.path.join(dst_dir, old_file_name)
        new_dst_file_name = os.path.join(dst_dir, new_file_name)
        os.rename(dst_file, new_dst_file_name)


def changeName():
    for code in shcode:
        codeName = "A"+code+".xlsx"
        copy_rename("A000000.xlsx",codeName)

if __name__ == '__main__':
    get_CSV()